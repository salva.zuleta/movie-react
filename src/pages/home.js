import React from 'react';
import { Row, Col } from "antd";
import useFetch from "../hooks/useFetch";
import {URL_API, API} from '../utils/contants';
import MovieList from '../components/MovieList';
import SliderMovies from "../components/SliderMovies";
import Footer from "../components/Footer";


export default function Home (){
	const newMovies = useFetch(
		`${URL_API}/movie/now_playing?api_key=${API}&language=en-ES&page=3`
		);
	const popularMovies = useFetch(
		`${URL_API}/movie/popular?api_key=${API}&language=en-ES&page=3`
		);
	const topRateMovies = useFetch(
		`${URL_API}/movie/top_rated?api_key=${API}&language=en-ES&page=3`
		);

	console.log(newMovies);
	return (
		<>
		<SliderMovies movies={newMovies} />
		<Row>
			<Col span={12}>
				<MovieList title="Peliculas Populares" 
				movies={popularMovies} />
			</Col>
			<Col span={12}>
				<MovieList title="Top Mejores Peliculas Puntuadas" 
				movies={topRateMovies} />
			</Col>
		</Row>
		<Footer />
		</>
		);
}